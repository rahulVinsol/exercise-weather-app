//
//  ViewModel.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 16/06/21.
//

import Foundation
import Combine

class ViewModel {
    @Published private(set) var weatherData: WeatherData?
    @Published private(set) var error: Error?
    
    private let networkInterfaceProtocol: NetworkInterfaceProtocol = NetworkInterface.shared
    private var cancellables = [AnyCancellable]()
    
    func fetchWeatherData(for cityName: String) {
        networkInterfaceProtocol.getWeatherData(for: cityName)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self?.error = error
                    break
                }
            }, receiveValue: { [weak self] response in
                self?.weatherData = response
            }).store(in: &cancellables)
    }
    
    deinit {
        cancellables.removeAll()
    }
}
