//
//  RemoteRepo.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 16/06/21.
//

import Foundation
import Combine
import Alamofire

fileprivate let BASE_URL = "https://api.openweathermap.org/data/2.5/weather"

protocol NetworkInterfaceProtocol {
    func getWeatherData(for cityName: String) -> AnyPublisher<WeatherData, Error>
}

class NetworkInterface: NetworkInterfaceProtocol {
    static let shared: NetworkInterface = NetworkInterface()
    
    private init() { }
    
    func getWeatherData(for cityName: String) -> AnyPublisher<WeatherData, Error> {
        AF.request(
            BASE_URL,
            method: .get,
            parameters: CurrentWeatherParams(q: cityName)
        )
        //validating the data type of response and sending failure iff the response returns success and json response is a custom failure response sent by open weather which is lost if another validate call is made before checking
        .validate { (urlRequest, response, data) in
            if !(200..<300).contains(response.statusCode) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: String]
                    
                    let code = Int(json?["cod"] ?? "")
                    let message = json?["message"]
                    
                    guard let code = code, let message = message else { return .success(()) }
                    
                    let networkError = CurrentWeatherError.networkError(code: code, message: message)
                    return .failure(networkError)
                } catch {
                    
                }
            }
            return .success(())
        }
        //default validate call of DataRequest that checks content types and acceptable status codes
        .validate()
        .publishDecodable(type: WeatherData.self)
        .value()
        .mapError { error in
            return error.underlyingError ?? error
        }
        .eraseToAnyPublisher()
    }
}
