//
//  ViewController.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 16/06/21.
//

import UIKit
import Combine

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var cloudsLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var dateTakenLabel: UILabel!
    
    private let viewModel = ViewModel()
    private var cancellables = [AnyCancellable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setObservers()
        
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        searchBar.delegate = self
    }
    
    private func setObservers() {
        viewModel.$weatherData.sink { [weak self] weatherData in
            guard let weatherData = weatherData else { return }
            
            self?.setWeatherData(weatherData: weatherData)
        }.store(in: &cancellables)
        
        viewModel.$error.sink { [weak self] error in
            guard let error = error else { return }
            
            switch error {
            case CurrentWeatherError.networkError(let code, let message):
                self?.showErrorAlertVC(title: "\(code)", message: message)
            default:
                self?.showErrorAlertVC(title: "Error", message: "Some error occured")
                break
            }
            
        }.store(in: &cancellables)
    }
    
    private func setWeatherData(weatherData: WeatherData) {
        cityLabel.text = "City:- \(weatherData.name)"
        weatherLabel.text = "Weather:- \(weatherData.weather[0].main)"
        tempLabel.text = "Temperature:- \(String(format: "%.2f", weatherData.main.temp - 273.15)) °C"
        feelsLikeLabel.text = "Feels Like:- \(String(format: "%.2f", weatherData.main.feelsLike - 273.15)) °C"
        humidityLabel.text = "Humidity:- \(weatherData.main.humidity)%"
        windSpeedLabel.text = "Wind Speed:- \(String(format: "%.2f", weatherData.wind.speed * 3.6)) kmph"
        cloudsLabel.text = "Clouds:- \(weatherData.clouds.all)%"
        sunriseLabel.text = "Sunrise:- \(weatherData.getSunriseDate().getFormattedDate())"
        sunsetLabel.text = "Sunset:- \(weatherData.getSunsetDate().getFormattedDate())"
        dateTakenLabel.text = "Date Taken:- \(weatherData.getCollectionDate().getFormattedDate())"
    }
    
    func showErrorAlertVC(title: String, message: String? = nil, error: Error? = nil) {
        let alert = UIAlertController(title: title, message: message ?? error?.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    deinit {
        cancellables.removeAll()
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text
        
        guard searchText?.isEmpty == false else {
            return
        }
    
        searchBar.resignFirstResponder()
        
        viewModel.fetchWeatherData(for: searchText! )
    }
}
