//
//  WeatherDataError.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 17/06/21.
//

import Foundation

enum CurrentWeatherError: LocalizedError {
    case networkError(code: Int, message: String)
    
    public var errorDescription: String? {
        switch self {
        case .networkError(_, let message):
            return NSLocalizedString(message, comment: message)
        }
    }
}
