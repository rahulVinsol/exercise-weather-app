//
//  QueryParameter.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 16/06/21.
//

import Foundation

let API_KEY = "f4c35fcd3f8ff1fce290647820bf4543"

struct CurrentWeatherParams: Encodable {
    let q: String
    let appid: String = API_KEY
}
