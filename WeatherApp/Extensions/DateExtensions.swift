//
//  DateExtensions.swift
//  WeatherApp
//
//  Created by Rahul Rawat on 17/06/21.
//

import Foundation

fileprivate let dateFormat = "hh:mm:ss a"

fileprivate let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    return formatter
}()

extension Date {
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func getFormattedDate() -> String {
        dateFormatter.string(from: self)
    }
}
